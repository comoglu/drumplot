from obspy import read
import sys
import os

#fisier = sys.argv[-1]
#root = "/bigdata/seiscomp_data/2016/RO/MLR/HHZ.D"

root = sys.argv[1]
if not os.path.isdir(root):
        print "invalid directory %s" % root
        sys.exit()


print "Scanning for miniseed in %s" % root

for root, dirs, files in os.walk(root, topdown=False):
    for file in files:
        try:
                if 'Z.D.' not in file:
                        continue
                outFile  = os.path.basename(file)+".png"
                st = read(os.path.join(root,file))
#                st.filter("bandpass", freqmin = 0.8 ,freqmax = 8.0)
#                st.filter("lowpass", freq=0.1, corners=2)
                st.plot(type="section", t_vertical_labels=False, vertical_scaling_range=5e3,interval=30, 
                        one_tick_per_line=True,color=['k', 'r', 'b', 'g'], show_y_UTC_label=True,
                        outfile=outFile , dpi=150,
                        size=(1300, 900), width=0.8,
                        title=os.path.basename(file))
        except Exception:
                print "Could not generate helicorder for {}".format(file)


'''
st = read(fisier)

outFile = os.path.basename(fisier)+".png"

st.filter("highpass", freq = 0.2, corners = 2)
st.plot(type="dayplot", interval=30, one_tick_per_line=True,
        color=['b', 'r'], outfile=outFile , dpi=150,
        size=(1300, 900), width=0.8)
'''
